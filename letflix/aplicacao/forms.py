from django import forms
from aplicacao.models import Conteudo, Filme, Serie

class InsereFilmeForm(forms.ModelForm):
  premiacao = forms.CharField(label='Premiação', required=False)
  #conteudo = forms.ModelChoiceField(queryset=Conteudo.objects.all(), empty_label=None)
  class Meta:
    model = Filme

    fields = [
      'premiacao',
   #   'conteudo',
    ]

class InsereConteudoForm(forms.ModelForm):
  classificacaoindicativa = forms.CharField(label='Classificação Indicativa', required=False)
  titulo = forms.CharField(label='Título', required=True)
  original = forms.IntegerField(label='É original?', required=True)
  descricao = forms.CharField(widget=forms.Textarea, label='Descrição', required=True)
  duracao = forms.IntegerField(label='Duração', required=True)
  distribuicao = forms.CharField(label='Distribuição', required=False)

  class Meta:
      model = Conteudo

      fields = [
        'classificacaoindicativa',
        'titulo',
        'original',
        'descricao',
        'duracao',
        'distribuicao',
      ]

class InsereSerieForm(forms.ModelForm):
  titulo = forms.CharField(label='Título', required=True)
  descricao_geral = forms.CharField(widget=forms.Textarea, label='Descrição', required=True)
  status = forms.CharField(label='Status', required=False)
  premiacao = forms.CharField(label='Premiação', required=False)

  class Meta:
    model = Serie

    fields = '__all__'

class InsereFilmeEConteudoForm(forms.Form):
  premiacao = forms.CharField(label='Premiação', required=False)
  classificacaoindicativa = forms.CharField(label='Classificação Indicativa', required=False)
  titulo = forms.CharField(label='Título', required=True)
  original = forms.IntegerField(label='É original?', required=True)
  descricao = forms.CharField(widget=forms.Textarea, label='Descrição', required=True)
  duracao = forms.IntegerField(label='Duração', required=True)
  distribuicao = forms.CharField(label='Distribuição', required=False)

  class Meta:
    fields = '__all__'


class InsereEpisodio(forms.Form):
  temporada = forms.IntegerField(label='Temporada', required=True)
  episodio = forms.IntegerField(label='Episódio', required=True)
  #conteudo
  classificacaoindicativa = forms.CharField(label='Classificação Indicativa', required=False)
  titulo = forms.CharField(label='Título', required=True)
  original = forms.IntegerField(label='É original?', required=True)
  descricao = forms.CharField(widget=forms.Textarea, label='Descrição', required=True)
  duracao = forms.IntegerField(label='Duração', required=True)
  distribuicao = forms.CharField(label='Distribuição', required=False)

  class Meta:
    fields = [
      'temporada',
      'episodio',
      'classificacaoindicativa',
      'titulo',
      'original',
      'descricao',
      'duracao',
      'distribuicao',
    ]