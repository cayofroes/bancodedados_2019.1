# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Ator(models.Model):
    nome = models.CharField(max_length=60)
    premiacao = models.CharField(max_length=120, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ator'


class Conta(models.Model):
    email = models.CharField(max_length=45)
    senhacontrole = models.CharField(db_column='senhaControle', max_length=255)  # Field name made lowercase.
    senhaacesso = models.CharField(db_column='senhaAcesso', max_length=255)  # Field name made lowercase.
    tipoconta = models.ForeignKey('Tipoconta', models.DO_NOTHING, db_column='TipoConta_id')  # Field name made lowercase.
    username = models.CharField(db_column='username', max_length=150)

    class Meta:
        managed = False
        db_table = 'conta'


class Conteudo(models.Model):
    classificacaoindicativa = models.CharField(db_column='classificacaoIndicativa', max_length=10, blank=True, null=True)  # Field name made lowercase.
    titulo = models.CharField(max_length=60)
    original = models.IntegerField()
    descricao = models.TextField()
    duracao = models.IntegerField()
    distribuicao = models.CharField(max_length=60, blank=True, null=True)

    class Meta:
        #managed = False
        db_table = 'conteudo'


class ConteudoTemAtor(models.Model):
    conteudo = models.ForeignKey(Conteudo, models.DO_NOTHING, db_column='Conteudo_id', primary_key=True)  # Field name made lowercase.
    ator = models.ForeignKey(Ator, models.DO_NOTHING, db_column='Ator_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'conteudo_tem_ator'
        unique_together = (('conteudo', 'ator'),)


class ConteudoTemGenero(models.Model):
    conteudo = models.ForeignKey(Conteudo, models.DO_NOTHING, db_column='Conteudo_id', primary_key=True)  # Field name made lowercase.
    genero = models.ForeignKey('Genero', models.DO_NOTHING, db_column='Genero_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'conteudo_tem_genero'
        unique_together = (('conteudo', 'genero'),)


class Episodio(models.Model):
    temporada = models.IntegerField()
    episodio = models.IntegerField()
    serie = models.ForeignKey('Serie', models.DO_NOTHING, db_column='Serie_id')  # Field name made lowercase.
    conteudo = models.ForeignKey(Conteudo, models.DO_NOTHING, db_column='Conteudo_id', primary_key=True)  # Field name made lowercase.

    class Meta:
        #managed = False
        db_table = 'episodio'


class Filme(models.Model):
    premiacao = models.CharField(max_length=120, blank=True, null=True)
    conteudo = models.ForeignKey(Conteudo, models.DO_NOTHING, db_column='Conteudo_id', primary_key=True)  # Field name made lowercase.

    class Meta:
        #managed = False
        db_table = 'filme'


class Genero(models.Model):
    nome = models.CharField(max_length=45)

    class Meta:
        #managed = False
        db_table = 'genero'


class Lista(models.Model):
    titulo = models.CharField(max_length=45)
    descricaolista = models.TextField(db_column='descricaoLista', blank=True, null=True)  # Field name made lowercase.
    perfil = models.ForeignKey('Perfil', models.DO_NOTHING, db_column='Perfil_id', related_name='perfil_id')  # Field name made lowercase.
    perfil_conta = models.ForeignKey('Perfil', models.DO_NOTHING, db_column='Perfil_Conta_id', related_name='perfil_conta_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'lista'
        unique_together = (('id', 'perfil', 'perfil_conta'),)


class ListaTemConteudo(models.Model):
    conteudo = models.ForeignKey(Conteudo, models.DO_NOTHING, db_column='Conteudo_id', primary_key=True)  # Field name made lowercase.
    lista = models.ForeignKey(Lista, models.DO_NOTHING, db_column='Lista_id', related_name='lista_id')  # Field name made lowercase.
    lista_perfil = models.ForeignKey(Lista, models.DO_NOTHING, db_column='Lista_Perfil_id', related_name='lista_perfil_id')  # Field name made lowercase.
    lista_perfil_conta = models.ForeignKey(Lista, models.DO_NOTHING, db_column='Lista_Perfil_Conta_id', related_name='lista_perfil_conta_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'lista_tem_conteudo'
        unique_together = (('conteudo', 'lista', 'lista_perfil', 'lista_perfil_conta'),)


class Pagamento(models.Model):
    mes = models.PositiveIntegerField(primary_key=True)
    formapagamento = models.CharField(db_column='formaPagamento', max_length=6)  # Field name made lowercase.
    datapagamento = models.DateField(db_column='dataPagamento')  # Field name made lowercase.
    conta = models.ForeignKey(Conta, models.DO_NOTHING, db_column='Conta_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'pagamento'
        unique_together = (('mes', 'conta'),)


class Perfil(models.Model):
    pin = models.CharField(max_length=255)
    nome = models.CharField(max_length=45)
    idade = models.IntegerField()
    icone = models.CharField(max_length=120, blank=True, null=True)
    controleparental = models.IntegerField(db_column='controleParental')  # Field name made lowercase.
    conta = models.ForeignKey(Conta, models.DO_NOTHING, db_column='Conta_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'perfil'
        unique_together = (('id', 'conta'),)


class Serie(models.Model):
    titulo = models.CharField(max_length=60)
    descricao_geral = models.TextField()
    status = models.CharField(max_length=10, blank=True, null=True)
    premiacao = models.CharField(max_length=120, blank=True, null=True)

    class Meta:
        #managed = False
        db_table = 'serie'


class SimilaridadeGenero(models.Model):
    generoa = models.ForeignKey(Genero, models.DO_NOTHING, db_column='GeneroA_id', primary_key=True, related_name='genero_a_id')  # Field name made lowercase.
    generob = models.ForeignKey(Genero, models.DO_NOTHING, db_column='GeneroB_id', related_name='genero_b_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'similaridade_genero'
        unique_together = (('generoa', 'generob'),)


class Stream(models.Model):
    nota = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
    duracao = models.IntegerField()
    data = models.DateField()
    conteudo = models.ForeignKey(Conteudo, models.DO_NOTHING, db_column='Conteudo_id', primary_key=True)  # Field name made lowercase.
    lista = models.ForeignKey(Lista, models.DO_NOTHING, db_column='Lista_id', blank=True, null=True)  # Field name made lowercase.
    perfil = models.ForeignKey(Perfil, models.DO_NOTHING, db_column='Perfil_id', related_name='perfil_stream_id')  # Field name made lowercase.
    perfil_conta = models.ForeignKey(Perfil, models.DO_NOTHING, db_column='Perfil_Conta_id', related_name='perfil_stream_conta_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'stream'
        unique_together = (('conteudo', 'perfil', 'perfil_conta'),)


class Tipoconta(models.Model):
    nomeconta = models.CharField(db_column='nomeConta', max_length=45)  # Field name made lowercase.
    qualidadestream = models.IntegerField(db_column='qualidadeStream')  # Field name made lowercase.
    limiteperfis = models.IntegerField(db_column='limitePerfis')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'tipoconta'