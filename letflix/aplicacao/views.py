from django.shortcuts import render, redirect
from aplicacao.models import Filme, Conteudo, Serie, Episodio
from django.views.generic import ListView, CreateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django import forms
from aplicacao.forms import *
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ObjectDoesNotExist

# Create your views here.

def index(request):
	return render(request, 'aplicacao/index.html', {})

def inicio(request):
	filme = Filme.objects.all()
	serie = Serie.objects.all()
	return render(request, 'aplicacao/inicio.html', {'filmes': filme, 'series': serie})

def saibamais(request, objeto, pk):
	if objeto == "filme" :
		filme = Filme.objects.get(pk=pk)
		return render(request, 'aplicacao/_layout/_infoFilme.html', {'filme': filme})
	else:
		serie = Serie.objects.get(pk=pk)
		return render(request, 'aplicacao/_layout/_infoSerie.html', {'serie': serie})

def criaFilme(request):
	# if this is a POST request we need to process the form data
	if request.method == 'POST':
		# create a form instance and populate it with data from the request:
		form_Filme = InsereFilmeEConteudoForm(request.POST, request.FILES)
		#form_Conteudo = InsereConteudoForm(request.POST)
		# check whether it's valid:
		if form_Filme.is_valid():
			conteudo = Conteudo()
			conteudo.titulo = form_Filme.cleaned_data['titulo']
			conteudo.descricao = form_Filme.cleaned_data['descricao']
			conteudo.duracao = form_Filme.cleaned_data['duracao']
			conteudo.original = form_Filme.cleaned_data['original']
			conteudo.distribuicao = form_Filme.cleaned_data['distribuicao']
			conteudo.classificacaoindicativa = form_Filme.cleaned_data['classificacaoindicativa']
			conteudo.save()
			filme = Filme()
			filme.premiacao = form_Filme.cleaned_data['premiacao']
			filme.conteudo = conteudo
			filme.save()
			return redirect('saibamais', objeto='filme', pk=filme.pk)
		
		# if a GET (or any other method) we'll create a blank form
	else:
		form_Filme = InsereFilmeEConteudoForm()
		
	return render(request, 'aplicacao/_layout/_createFilmeCompleto.html', {'form_Filme': form_Filme})

def atualizaFilme(request, pk):
# if this is a POST request we need to process the form data
	if request.method == 'POST':
		# create a form instance and populate it with data from the request:
		form_Filme = InsereFilmeEConteudoForm(request.POST)
		#form_Conteudo = InsereConteudoForm(request.POST)
		# check whether it's valid:
		if form_Filme.is_valid():
			filme = Filme.objects.get(pk=pk)
			filme.conteudo.titulo = form_Filme.cleaned_data['titulo']
			filme.conteudo.descricao = form_Filme.cleaned_data['descricao']
			filme.conteudo.duracao = form_Filme.cleaned_data['duracao']
			filme.conteudo.original = form_Filme.cleaned_data['original']
			filme.conteudo.distribuicao = form_Filme.cleaned_data['distribuicao']
			filme.conteudo.classificacaoindicativa = form_Filme.cleaned_data['classificacaoindicativa']
			filme.conteudo.save()
			filme.premiacao = form_Filme.cleaned_data['premiacao']
			filme.save()
			return redirect('saibamais', objeto='filme', pk=filme.pk)
		
		# if a GET (or any other method) we'll create a blank form
	else:
		filme = Filme.objects.get(pk=pk)
		form_Filme = InsereFilmeEConteudoForm(initial={'premiacao': filme.premiacao,
			'titulo': filme.conteudo.titulo, 'descricao': filme.conteudo.descricao,
			'classificacaoindicativa': filme.conteudo.classificacaoindicativa,
			'duracao': filme.conteudo.duracao, 'original': filme.conteudo.original,
			'distribuicao': filme.conteudo.distribuicao})
		
	return render(request, 'aplicacao/_layout/_updateFilmeCompleto.html', {'form_Filme': form_Filme})		

def criaEpisodio(request, pk):

	if request.method == 'POST':
		# create a form instance and populate it with data from the request:
		form = InsereEpisodio(request.POST)
		#form_Conteudo = InsereConteudoForm(request.POST)
		# check whether it's valid:
		if form.is_valid():
			conteudo = Conteudo()
			conteudo.titulo = form.cleaned_data['titulo']
			conteudo.descricao = form.cleaned_data['descricao']
			conteudo.duracao = form.cleaned_data['duracao']
			conteudo.original = form.cleaned_data['original']
			conteudo.distribuicao = form.cleaned_data['distribuicao']
			conteudo.classificacaoindicativa = form.cleaned_data['classificacaoindicativa']
			conteudo.save()
			episodio = Episodio()
			episodio.temporada = form.cleaned_data['temporada']
			episodio.episodio = form.cleaned_data['episodio']
			episodio.serie = Serie.objects.get(pk=pk)
			episodio.conteudo = conteudo
			episodio.save()
			return redirect('listaEpisodio', pk=episodio.serie.pk)
		
		# if a GET (or any other method) we'll create a blank form
	else:
		form = InsereEpisodio()
		
	return render(request, 'aplicacao/_layout/_createEpisodio.html', {'form': form})

def listaEpisodio(request, pk):
	try:
		serie = Serie.objects.get(pk=pk)
		print(serie.pk)
		episodio = Episodio.objects.filter(serie__pk=pk)
		return render(request, 'aplicacao/_layout/_listEpisodios.html', {'episodio':episodio, 'serie':serie})
	except Episodio.DoesNotExist:
		#return render(request, 'aplicacao/_layout/_listEpisodios.html', {'serie':serie})
		return redirect('criaEpisodio', pk=serie.pk)

def atualizaEpisodio(request, pk):
	if request.method == 'POST':
		# create a form instance and populate it with data from the request:
		form = InsereEpisodio(request.POST)
		#form_Conteudo = InsereConteudoForm(request.POST)
		# check whether it's valid:
		if form.is_valid():
			episodio = Episodio.objects.get(pk=pk)
			episodio.conteudo.titulo = form.cleaned_data['titulo']
			episodio.conteudo.descricao = form.cleaned_data['descricao']
			episodio.conteudo.duracao = form.cleaned_data['duracao']
			episodio.conteudo.original = form.cleaned_data['original']
			episodio.conteudo.distribuicao = form.cleaned_data['distribuicao']
			episodio.conteudo.classificacaoindicativa = form.cleaned_data['classificacaoindicativa']
			episodio.conteudo.save()
			episodio.temporada = form.cleaned_data['temporada']
			episodio.episodio = form.cleaned_data['episodio']
			episodio.save()
			return redirect('listaEpisodio', pk=episodio.serie.pk)
		
		# if a GET (or any other method) we'll create a blank form
	else:
		episodio = Episodio.objects.get(pk=pk)
		form = InsereEpisodio(initial={'titulo': episodio.conteudo.titulo,
			'descricao': episodio.conteudo.descricao, 'classificacaoindicativa':episodio.conteudo.classificacaoindicativa,
			'duracao':episodio.conteudo.duracao,
			'original':episodio.conteudo.original, 'distribuicao':episodio.conteudo.distribuicao,
			'temporada':episodio.temporada, 'episodio':episodio.episodio})
		
	return render(request, 'aplicacao/_layout/_atualizaEpisodio.html', {'form': form})

class EpisodioDeleteView(DeleteView):
	template_name = "aplicacao/_layout/_deleteEpisodio.html"
	model = Episodio
	success_url = reverse_lazy('series/')
	context_object_name = 'episodio'

class FilmeListView(ListView):
	template_name = "aplicacao/_layout/_listFilme.html"
	model = Filme
	context_object_name = "filme"

class SerieListView(ListView):
	template_name = "aplicacao/_layout/_listSerie.html"
	model = Serie
	context_object_name = "serie"

class ConteudoListView(ListView):
	template_name = "aplicacao/_layout/_listConteudo.html"
	model = Conteudo
	context_object_name = "conteudo"

class ConteudoCreateView(CreateView):
	template_name = "aplicacao/_layout/_createConteudo.html"
	model = Conteudo
	form_class = InsereConteudoForm
	success_url = reverse_lazy("createFilme")
	context_object_name = "conteudo"

class SerieCreateView(CreateView):
	template_name = "aplicacao/_layout/_createSerie.html"
	model = Serie
	form_class = InsereSerieForm
	success_url = reverse_lazy("series/")
	context_object_name = "serie"

class ConteudoUpdateView(UpdateView):
    template_name = "aplicacao/_layout/_updateConteudo.html"
    model = Conteudo
    fields = '__all__'
    context_object_name = 'conteudo'
    success_url = reverse_lazy("conteudos/")

class SerieUpdateView(UpdateView):
	template_name = "aplicacao/_layout/_updateSerie.html"
	model = Serie
	fields = '__all__'
	context_object_name = 'serie'
	success_url = reverse_lazy("series/")

class FilmeDeleteView(DeleteView):
	template_name = "aplicacao/_layout/_deleteFilme.html"
	model = Filme
	success_url = reverse_lazy('filmes/')
	context_object_name = 'filme'

class SerieDeleteView(DeleteView):
	template_name = "aplicacao/_layout/_deleteSerie.html"
	model = Serie
	success_url = reverse_lazy('series/')
	context_object_name = 'serie'

class SignUp(CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'registration/signup.html'