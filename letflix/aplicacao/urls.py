from django.urls import path, include
from . import views
from aplicacao.models import *

urlpatterns = [
    path('', views.index, name='index'),
    path('inicio', views.inicio, name='inicio'),
    path('saibamais/<objeto>/<pk>', views.saibamais, name='saibamais'),
	path('filmes/', views.FilmeListView.as_view(), name='filmes/'),
	path('deleteFilme/<pk>', views.FilmeDeleteView.as_view(), name='deleteFilme'),
	path('createConteudo', views.ConteudoCreateView.as_view(), name='createConteudo'),
	path('updateConteudo/<pk>', views.ConteudoUpdateView.as_view(), name='updateConteudo'),
	path('series/', views.SerieListView.as_view(), name='series/'),
	path('createSerie', views.SerieCreateView.as_view(), name='createSerie'),
	path('updateSerie/<pk>', views.SerieUpdateView.as_view(), name='updateSerie'),
	path('deleteSerie/<pk>', views.SerieDeleteView.as_view(), name='deleteSerie'),
	path('conteudos/', views.ConteudoListView.as_view(), name='conteudos/'),
	path('criaFilme', views.criaFilme, name='criaFilme'),
	path('atualizaFilme/<pk>', views.atualizaFilme, name='atualizaFilme'),
	path('access/', include('django.contrib.auth.urls')),
	path('signup/', views.SignUp.as_view(), name='signup'),
	path('criaEpisodio/<pk>', views.criaEpisodio, name='criaEpisodio'),
	path('listaEpisodio/<pk>', views.listaEpisodio, name='listaEpisodio'),
	path('deleteEpisodio/<pk>', views.EpisodioDeleteView.as_view(), name='deleteEpisodio'),
	path('updateEpisodio/<pk>', views.atualizaEpisodio, name='updateEpisodio'),
]