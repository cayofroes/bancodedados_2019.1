--A Lista os titulos que possuem maior duracao que um titulo em específico 
select titulo from Conteudo
where duracao > (select duracao from Conteudo where titulo 
like '%parte unica do titulo da serie%');
--B Lista quantas series e filmes possuem nota maior ou igual a 7 
select count(nota) from Stream where nota >= 7;
--C Lista a quantidade de titulos por cada distribuidora em ordem decrescente
select count(titulo), distribuicao from Conteudo 
Group By distribuicao order by count(titulo) desc;
--D Lista a nota, duracao e titulo das series e filmes
select S.nota, S.duracao, C.titulo 
from Stream as S left join Conteudo as C on S.Conteudo_id = C.id; 
--E seleciona todas series que tem algum status
select titulo from Serie where status is not null
--F Lista todos atores que o nome comeca com A 
select nome from Ator where nome like 'a%'
--G Lista todos os filmes de um gênero específico
select C.titulo as 'Titulo do Filme' 
from Filme as F inner join Conteudo as C on C.id = F.Conteudo_id 
inner join conteudo_tem_genero as CG on CG.Conteudo_id = F.Conteudo_id 
inner join Genero as G on G.id = CG.Genero_id where G.nome like '%Action%'