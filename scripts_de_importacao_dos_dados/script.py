import csv
import os

with open("arquivo.csv","rb") as csv_file:
	csv_reader = csv.reader(csv_file, delimiter=',', lineterminator='\n')
	with open("result.csv","wb") as result:
        	output_wrt = csv.writer(result, delimiter=',', lineterminator='\n')
        	for r in csv_reader:
            		output_wrt.writerow( (r[3], r[6], r[9], r[10], r[11], r[14], r[21]) )

with open("result.csv","rb") as clean_csv:
	reader = csv.reader(clean_csv, delimiter=',', lineterminator='\n')
	with open("result_clean.csv","wb") as output:
		writer = csv.writer(output, delimiter=',', lineterminator='\n')
		for row in reader:
			if ( not ((row[0] in (None, "")) 
				or (row[1] in (None, "")) 
				or (row[2] in (None, "")) 
				or (row[3] in (None, "")) 
				or (row[4] in (None, ""))
				or (row[5] in (None, ""))
				or (row[6] in (None, "")))):
				writer.writerow( (row[0], row[1], row[2], row[3], row[4], row[5], row[6]) )

os.system("cp result_clean.csv original.csv")

with open("result_clean.csv","rb") as csv_all:
	reader = csv.reader(csv_all, delimiter=',', lineterminator='\n')
	with open("generos.csv","wb") as output_generos:
		writer = csv.writer(output_generos, delimiter=',', lineterminator='\n')
		for row in reader:
			writer.writerow(row[2].split('|'))

with open("result_clean.csv","rb") as csv_all:
	reader = csv.reader(csv_all, delimiter=',', lineterminator='\n')
	with open("atores.csv","wb") as output_atores:
		writer = csv.writer(output_atores, delimiter=',', lineterminator='\n')
		for row in reader:
			writer.writerow((row[1], row[3], row[5]))

with open("result_clean.csv","rb") as csv_all:
	reader = csv.reader(csv_all, delimiter=',', lineterminator='\n')
	with open("conteudo.csv","wb") as output_conteudo:
		writer = csv.writer(output_conteudo, delimiter=',', lineterminator='\n')
		for row in reader:
			writer.writerow((row[6], row[4], '2', 'sem descricao', row[0]))

with open("generos.csv","rb") as csv_generos:
	reader = csv.reader(csv_generos, delimiter=',', lineterminator='\n')
	with open("insert_generos.csv","wb") as output_generos_insert:
		writer = csv.writer(output_generos_insert, delimiter=',', lineterminator='\n')
		for row in reader:
			if len(row) == 1:
				writer.writerow( ([row[0]]) )
			else:
				for col in row:
					writer.writerow( ([col]) )
os.system("grep -v 'genres' insert_generos.csv > all_generos_clean.csv")
os.system("sort -u all_generos_clean.csv -o all_generos_uniq.csv")

os.system("grep -v 'actor_2' atores.csv > atores_clean.csv")
with open("atores_clean.csv","rb") as csv_atores:
	reader = csv.reader(csv_atores, delimiter=',', lineterminator='\n')
	with open("insert_atores.csv","wb") as output_atores_insert:
		writer = csv.writer(output_atores_insert, delimiter=',', lineterminator='\n')
		for row in reader:
			if len(row) == 1:
				writer.writerow( ([row[0]]) )
			else:
				for col in row:
					writer.writerow( ([col]) )
os.system("sort -u insert_atores.csv -o all_atores_uniq.csv")

os.system("rm atores_clean.csv insert_atores.csv result_clean.csv result.csv insert_generos.csv all_generos_clean.csv")
