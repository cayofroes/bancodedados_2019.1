SELECT id, nome
FROM Genero
INTO OUTFILE '/var/lib/mysql-files/genero_nome_id.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n';
