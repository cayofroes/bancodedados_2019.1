LOAD DATA LOCAL INFILE 'scripts_de_importacao_dos_dados/all_generos_uniq.csv' INTO TABLE Genero 
FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n'  
(@col1) set nome=@col1 ;

LOAD DATA LOCAL INFILE 'scripts_de_importacao_dos_dados/all_atores_uniq.csv' INTO TABLE Ator 
FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n'  
(@col1) set nome=@col1 ;

LOAD DATA LOCAL INFILE 'scripts_de_importacao_dos_dados/conteudo.csv' INTO TABLE Conteudo 
FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n'
IGNORE 1 LINES
(@col1, @col2, @col3, @col4, @col5) set classificacaoIndicativa=@col1, titulo=@col2, original=@col3, descricao=@col4, duracao=@col5 ;
