import csv
import os

with open("original.csv", 'rb') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',', lineterminator='\n')
    original = list(list(rec) for rec in csv_reader)

with open("conteudo_titulo_id.csv", 'rb') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',', lineterminator='\n')
    conteudo = list(list(rec) for rec in csv_reader)    

with open("genero_nome_id.csv", 'rb') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',', lineterminator='\n')
    generos = list(list(rec) for rec in csv_reader)

with open("ator_nome_id.csv", 'rb') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',', lineterminator='\n')
    atores = list(list(rec) for rec in csv_reader)

del original[0]

original_clean = []

for line in original:
    new_line = line[1:-1]
    original_clean.append(new_line)

for i in range(len(original_clean)):
    original_clean[i][1] = original_clean[i][1].split("|")
    original_clean[i][0] = [original_clean[i][0], original_clean[i][2], original_clean[i][4]]
    original_clean[i][2] = i + 1
    original_clean[i] = original_clean[i][:-2]

ator_conteudo = []
genero_conteudo = []
for line in original_clean:
    for ator in line[0]:
        for tupla in atores:
            if ator == tupla[1]:
                ator_conteudo.append([tupla[0], line[2]])
                break
    for genero in line[1]:
        for tupla in generos:
            if genero == tupla[1]:
                genero_conteudo.append([tupla[0], line[2]])
                break

arquivo_ator = ""
arquivo_genero = ""

for ids_ator in ator_conteudo:
    arquivo_ator += str(ids_ator[0]) + "," + str(ids_ator[1]) + '\n'

for ids_genero in genero_conteudo:
    arquivo_genero += str(ids_genero[0]) + "," + str(ids_genero[1]) + '\n'

with open("relac_ator_conteudo.csv","w") as f:
    f.write(arquivo_ator)

with open("relac_genero_conteudo.csv","w") as f:
    f.write(arquivo_genero)

