LOAD DATA LOCAL INFILE 'scripts_de_importacao_dos_dados/relac_ator_conteudo.csv' INTO TABLE Conteudo_tem_Ator
FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n'
(@col1, @col2) set Ator_id=@col1, Conteudo_id=@col2 ;

LOAD DATA LOCAL INFILE 'scripts_de_importacao_dos_dados/relac_genero_conteudo.csv' INTO TABLE Conteudo_tem_Genero
FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n'
(@col1, @col2) set Genero_id=@col1, Conteudo_id=@col2 ;
