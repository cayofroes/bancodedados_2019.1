CREATE SCHEMA letflix DEFAULT CHARACTER SET utf8;

USE letflix;

CREATE TABLE letflix.TipoConta (
  id INT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  nomeConta VARCHAR(45) NOT NULL,
  qualidadeStream INT(3) NOT NULL,
  limitePerfis INT(3) NOT NULL,
  PRIMARY KEY (id))
ENGINE = InnoDB;


CREATE TABLE letflix.Conta (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  email VARCHAR(45) NOT NULL,
  senhaControle VARCHAR(255) NOT NULL,
  senhaAcesso VARCHAR(255) NOT NULL,
  TipoConta_id INT(3) UNSIGNED NOT NULL,
  username VARCHAR(150) NOT NULL,
  PRIMARY KEY (id),
  INDEX fk_Conta_TipoConta1_idx (TipoConta_id ASC),
  CONSTRAINT fk_Conta_TipoConta1
    FOREIGN KEY (TipoConta_id)
    REFERENCES letflix.TipoConta (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE letflix.Perfil (
  id INT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  pin VARCHAR(255) NOT NULL,
  nome VARCHAR(45) NOT NULL,
  idade TINYINT(3) NOT NULL,
  icone VARCHAR(120) NULL,
  controleParental TINYINT(1) NOT NULL,
  Conta_id INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (id, Conta_id),
  INDEX fk_Perfil_Conta1_idx (Conta_id ASC),
  CONSTRAINT fk_Perfil_Conta1
    FOREIGN KEY (Conta_id)
    REFERENCES letflix.Conta (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE letflix.Conteudo (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  classificacaoIndicativa VARCHAR(10) NULL,
  titulo VARCHAR(255) NOT NULL,
  original TINYINT(1) NOT NULL,
  descricao MEDIUMTEXT NOT NULL,
  duracao INT(5) NOT NULL,
  distribuicao VARCHAR(60) NULL,
  PRIMARY KEY (id))
ENGINE = InnoDB;


CREATE TABLE letflix.Genero (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  nome VARCHAR(45) NOT NULL,
  PRIMARY KEY (id))
ENGINE = InnoDB;


CREATE TABLE letflix.Ator (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  nome VARCHAR(60) NOT NULL,
  premiacao VARCHAR(120) NULL,
  PRIMARY KEY (id))
ENGINE = InnoDB;


CREATE TABLE letflix.Serie (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  titulo VARCHAR(60) NOT NULL,
  descricao_geral MEDIUMTEXT NOT NULL,
  status ENUM('andamento', 'finalizada', 'hiatus') NULL,
  premiacao VARCHAR(120) NULL,
  PRIMARY KEY (id))
ENGINE = InnoDB;


CREATE TABLE letflix.Lista (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  titulo VARCHAR(45) NOT NULL,
  descricaoLista TINYTEXT NULL,
  Perfil_id INT(3) UNSIGNED NOT NULL,
  Perfil_Conta_id INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (id, Perfil_id, Perfil_Conta_id),
  INDEX fk_Lista_Perfil1_idx (Perfil_id ASC, Perfil_Conta_id ASC),
  CONSTRAINT fk_Lista_Perfil1
    FOREIGN KEY (Perfil_id , Perfil_Conta_id)
    REFERENCES letflix.Perfil (id , Conta_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE letflix.Pagamento (
  mes INT(5) UNSIGNED NOT NULL,
  formaPagamento ENUM('cartao', 'boleto') NOT NULL,
  dataPagamento DATE NOT NULL,
  Conta_id INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (mes, Conta_id),
  INDEX fk_Pagamento_Conta1_idx (Conta_id ASC),
  CONSTRAINT fk_Pagamento_Conta1
    FOREIGN KEY (Conta_id)
    REFERENCES letflix.Conta (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE letflix.Lista_tem_Conteudo (
  Conteudo_id INT(11) UNSIGNED NOT NULL,
  Lista_id INT(11) UNSIGNED NOT NULL,
  Lista_Perfil_id INT(3) UNSIGNED NOT NULL,
  Lista_Perfil_Conta_id INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (Conteudo_id, Lista_id, Lista_Perfil_id, Lista_Perfil_Conta_id),
  INDEX fk_lista_tem_conteudo_conteudo1_idx (Conteudo_id ASC),
  INDEX fk_Lista_tem_Conteudo_Lista1_idx (Lista_id ASC, Lista_Perfil_id ASC, Lista_Perfil_Conta_id ASC),
  CONSTRAINT fk_lista_tem_conteudo_conteudo1
    FOREIGN KEY (Conteudo_id)
    REFERENCES letflix.Conteudo (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_Lista_tem_Conteudo_Lista1
    FOREIGN KEY (Lista_id , Lista_Perfil_id , Lista_Perfil_Conta_id)
    REFERENCES letflix.Lista (id , Perfil_id , Perfil_Conta_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE letflix.Episodio (
  temporada TINYINT(3) NOT NULL,
  episodio INT(5) NOT NULL,
  Serie_id INT(11) UNSIGNED NOT NULL,
  Conteudo_id INT(11) UNSIGNED NOT NULL,
  INDEX fk_Episodio_Serie_Serie1_idx (Serie_id ASC),
  PRIMARY KEY (Conteudo_id),
  CONSTRAINT fk_Episodio_Serie_Serie1
    FOREIGN KEY (Serie_id)
    REFERENCES letflix.Serie (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_Episodio_Serie_Conteudo1
    FOREIGN KEY (Conteudo_id)
    REFERENCES letflix.Conteudo (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE letflix.Stream (
  nota DECIMAL NULL,
  duracao INT(5) NOT NULL,
  data DATE NOT NULL,
  Conteudo_id INT(11) UNSIGNED NOT NULL,
  Lista_id INT(11) UNSIGNED NULL,
  Perfil_id INT(3) UNSIGNED NOT NULL,
  Perfil_Conta_id INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (Conteudo_id, Perfil_id, Perfil_Conta_id),
  INDEX fk_Perfil_tem_Conteudo_Conteudo1_idx (Conteudo_id ASC),
  INDEX fk_Stream_Lista1_idx (Lista_id ASC),
  INDEX fk_Stream_Perfil1_idx (Perfil_id ASC, Perfil_Conta_id ASC),
  CONSTRAINT fk_Perfil_tem_Conteudo_Conteudo1
    FOREIGN KEY (Conteudo_id)
    REFERENCES letflix.Conteudo (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_Stream_Lista1
    FOREIGN KEY (Lista_id)
    REFERENCES letflix.Lista (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_Stream_Perfil1
    FOREIGN KEY (Perfil_id , Perfil_Conta_id)
    REFERENCES letflix.Perfil (id , Conta_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE letflix.Conteudo_tem_Genero (
  Conteudo_id INT(11) UNSIGNED NOT NULL,
  Genero_id INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (Conteudo_id, Genero_id),
  INDEX fk_Conteudo_tem_Genero_Genero1_idx (Genero_id ASC),
  INDEX fk_Conteudo_tem_Genero_Conteudo1_idx (Conteudo_id ASC),
  CONSTRAINT fk_Conteudo_tem_Genero_Conteudo1
    FOREIGN KEY (Conteudo_id)
    REFERENCES letflix.Conteudo (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_Conteudo_tem_Genero_Genero1
    FOREIGN KEY (Genero_id)
    REFERENCES letflix.Genero (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE letflix.Similaridade_Genero (
  GeneroA_id INT(11) UNSIGNED NOT NULL,
  GeneroB_id INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (GeneroA_id, GeneroB_id),
  INDEX fk_Genero_tem_Genero_Genero2_idx (GeneroB_id ASC),
  INDEX fk_Genero_tem_Genero_Genero1_idx (GeneroA_id ASC),
  CONSTRAINT fk_Genero_tem_Genero_Genero1
    FOREIGN KEY (GeneroA_id)
    REFERENCES letflix.Genero (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_Genero_tem_Genero_Genero2
    FOREIGN KEY (GeneroB_id)
    REFERENCES letflix.Genero (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE letflix.Conteudo_tem_Ator (
  Conteudo_id INT(11) UNSIGNED NOT NULL,
  Ator_id INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (Conteudo_id, Ator_id),
  INDEX fk_Conteudo_tem_Ator_Ator1_idx (Ator_id ASC),
  INDEX fk_Conteudo_tem_Ator_Conteudo1_idx (Conteudo_id ASC),
  CONSTRAINT fk_Conteudo_tem_Ator_Conteudo1
    FOREIGN KEY (Conteudo_id)
    REFERENCES letflix.Conteudo (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_Conteudo_tem_Ator_Ator1
    FOREIGN KEY (Ator_id)
    REFERENCES letflix.Ator (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE letflix.Filme (
  premiacao VARCHAR(120) NULL,
  Conteudo_id INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (Conteudo_id),
  CONSTRAINT fk_Filme_Conteudo
    FOREIGN KEY (Conteudo_id)
    REFERENCES letflix.Conteudo (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
